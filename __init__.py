from flask import Flask


def create_app():
	app = Flask(__name__)

	from . import index
	app.register_blueprint(index.bp)
	app.add_url_rule('/', endpoint='index')

	from . import prospectos
	app.register_blueprint(prospectos.bp)

	from . import vacantes
	app.register_blueprint(vacantes.bp)

	return app
