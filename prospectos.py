from flask import Blueprint, render_template, request
import pymssql
import json

bp = Blueprint('prospectos', __name__)


msg_error = "Error en la conexión. Consulte a su administrador."
def connection():
	try:
		connection = pymssql.connect(host='192.168.1.88', user='Polo-PC\\Polo', password='12345', database='entrevista')
		return connection
	except Exception as e:
		return None

@bp.route('/prospectos')
def prospectos():
	conn = connection()
	if conn == None:
		return msg_error
	
	cursor = conn.cursor(as_dict=True)
	cursor.execute('SELECT * FROM dbo.PROSPECTO')
	data = cursor.fetchall()
	cursor.close()
	conn.close()
	return render_template('prospectos/prospectos.html', datos=data)


@bp.route('/insertar_prospecto', methods = ['POST'])
def insertar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['id'] == "" or contenido['nombre'] == "" or contenido['correo'] == "" or contenido['fecha_registro'] == "":
		return json.dumps(["Uno o mas elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""INSERT INTO dbo.PROSPECTO 
		(id, nombre, correo, fecha_registro) 
		VALUES (%d, %s, %s, %s)""", 
		[(contenido['id'], 
		contenido['nombre'], 
		contenido['correo'], 
		contenido['fecha_registro'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Prospecto insertado exitosamente.", "success"])


@bp.route('/actualizar_prospecto', methods = ['POST'])
def actualizar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['nombre'] == "" or contenido['correo'] == "" or contenido['fecha_registro'] == "":
		return json.dumps(["Uno o mas elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""UPDATE dbo.PROSPECTO 
		SET nombre = %s, 
		correo = %s, 
		fecha_registro = %s 
		WHERE id = %d""", 
		[(contenido['nombre'], 
		contenido['correo'], 
		contenido['fecha_registro'], 
		contenido['id'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Prospecto actualizado exitosamente.", "success"])


@bp.route('/eliminar_prospecto', methods = ['POST'])
def eliminar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	try:
		cursor = conn.cursor()
		cursor.executemany("DELETE FROM dbo.PROSPECTO WHERE id = %d", [(contenido['id'])])
		conn.commit()
		cursor.close()
		conn.close()
	except Exception as e:
		return json.dumps(["El prospecto se encuentra asignado a una entrevista.", "error"])
	return json.dumps(["Prospecto eliminado exitosamente.", "success"])