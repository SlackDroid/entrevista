from flask import Blueprint, render_template, request
import pymssql
import json

bp = Blueprint('index', __name__)


msg_error = "Error en la conexión. Consulte a su administrador."
def connection():
	try:
		connection = pymssql.connect(host='192.168.1.88', user='Polo-PC\\Polo', password='12345', database='entrevista')
		return connection
	except Exception as e:
		return None

@bp.route('/')
def index():
	conn = connection()
	if conn == None:
		return msg_error
	
	dataEntrevistas, dataVacantes, dataProspectos = obtener_valores(conn)
	return render_template('index/index.html', entrevistas=dataEntrevistas, vacantes=dataVacantes, prospectos=dataProspectos)


def obtener_valores(conn):
	cursor = conn.cursor(as_dict=True)
	cursor.execute("""SELECT 
		dbo.VACANTE.area,
		dbo.PROSPECTO.nombre,
		dbo.ENTREVISTA.id,
		dbo.ENTREVISTA.vacante,
		dbo.ENTREVISTA.prospecto,
		dbo.ENTREVISTA.fecha_entrevista,
		dbo.ENTREVISTA.notas,
		dbo.ENTREVISTA.reclutado
		FROM dbo.ENTREVISTA
		INNER JOIN dbo.VACANTE
		ON dbo.ENTREVISTA.vacante = dbo.VACANTE.id
		INNER JOIN dbo.PROSPECTO
		ON dbo.ENTREVISTA.prospecto = dbo.PROSPECTO.id
		""")
	dataEntrevistas = cursor.fetchall()

	cursor.execute('SELECT id, area FROM dbo.VACANTE')
	dataVacantes = cursor.fetchall()

	cursor.execute('SELECT id, nombre FROM dbo.PROSPECTO')
	dataProspectos = cursor.fetchall()

	cursor.close()
	conn.close()
	return(dataEntrevistas, dataVacantes, dataProspectos)


@bp.route('/insertar_entrevista', methods = ['POST'])
def insertar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['id'] == "" or contenido['vacante'] == None or contenido['prospecto'] == None or contenido['fecha_entrevista'] == "":
		return json.dumps(["Uno o más elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""INSERT INTO dbo.ENTREVISTA 
		(id, vacante, prospecto, fecha_entrevista, notas, reclutado) 
		VALUES (%d, %d, %d, %s, %s, %s)""", 
		[(contenido['id'], 
		contenido['vacante'], 
		contenido['prospecto'], 
		contenido['fecha_entrevista'], 
		contenido['notas'], 
		contenido['reclutado'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Entrevista insertada exitosamente.", "success"])


@bp.route('/actualizar_entrevista', methods = ['POST'])
def actualizar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['vacante'] == None or contenido['prospecto'] == None or contenido['fecha_entrevista'] == "":
		return json.dumps(["Uno o más elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""UPDATE dbo.ENTREVISTA 
		SET vacante = %d, 
		prospecto = %d, 
		fecha_entrevista = %s, 
		notas = %s, 
		reclutado = %s WHERE id = %d""", 
		[(contenido['vacante'], 
		contenido['prospecto'], 
		contenido['fecha_entrevista'],
		contenido['notas'], 
		contenido['reclutado'],
		contenido['id'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Entrevista actualizada exitosamente.", "success"])


@bp.route('/eliminar_entrevista', methods = ['POST'])
def eliminar_prospecto():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	cursor = conn.cursor()
	cursor.executemany("DELETE FROM dbo.ENTREVISTA WHERE id = %d", [(contenido['id'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps("Entrevista eliminada exitosamente.")

