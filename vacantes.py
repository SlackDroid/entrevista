from flask import Blueprint, render_template, request
import pymssql
import json

bp = Blueprint('vacantes', __name__)


msg_error = "Error en la conexión. Consulte a su administrador."
def connection():
	try:
		connection = pymssql.connect(host='192.168.1.88', user='Polo-PC\\Polo', password='12345', database='entrevista')
		return connection
	except Exception as e:
		return None

@bp.route('/vacantes')
def vacantes():
	conn = connection()
	if conn == None:
		return msg_error
	
	cursor = conn.cursor(as_dict=True)
	cursor.execute('SELECT * FROM dbo.VACANTE')
	data = cursor.fetchall()
	cursor.close()
	conn.close()
	return render_template('vacantes/vacantes.html', datos=data)



@bp.route('/insertar_vacante', methods = ['POST'])
def insertar_vacante():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['id'] == "" or contenido['area'] == "" or contenido['sueldo'] == "":
		return json.dumps(["Uno o mas elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""INSERT INTO dbo.VACANTE 
		(id, area, sueldo, activo) 
		VALUES (%d, %s, %s, %s)""", 
		[(contenido['id'], 
		contenido['area'], 
		contenido['sueldo'], 
		contenido['activo'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Vacante insertada exitosamente.", "success"])


@bp.route('/actualizar_vacante', methods = ['POST'])
def modificar_vacante():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	if contenido['area'] == "" or contenido['sueldo'] == "":
		return json.dumps(["Uno o mas elementos obligatorios no han sido especificados", "warning"])

	cursor = conn.cursor()
	cursor.executemany("""UPDATE dbo.VACANTE 
		SET area = %s, 
		sueldo = %s, 
		activo = %s 
		WHERE id = %d""", 
		[(contenido['area'], 
		contenido['sueldo'], 
		contenido['activo'], 
		contenido['id'])])
	conn.commit()
	cursor.close()
	conn.close()
	return json.dumps(["Vacante actualizada exitosamente.", "success"])


@bp.route('/eliminar_vacante', methods = ['POST'])
def eliminar_vacante():
	contenido = request.get_json()
	conn = connection()
	if conn == None:
		return msg_error

	try:
		cursor = conn.cursor()
		cursor.executemany("DELETE FROM dbo.VACANTE WHERE id = %d", [(contenido['id'])])
		conn.commit()
		cursor.close()
		conn.close()
	except Exception as e:
		return json.dumps(["La vacante se encuentra asignada a una entrevista.", "error"])
	return json.dumps(["Vacante eliminada exitosamente.", "success"])
